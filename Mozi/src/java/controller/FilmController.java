/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Film;
import org.json.JSONArray;
import org.json.JSONObject;
import service.FilmService;

/**
 *
 * @author zsenakistvan
 */
public class FilmController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        FilmService filmService = new FilmService();
        try{
            if(request.getParameter("feladat") != null){
                if(request.getParameter("feladat").equals("mentes")){
                    if(request.getParameter("cim") != null &&
                       request.getParameter("hossz") != null &&
                       request.getParameter("korhatar") != null &&
                       request.getParameter("leiras") != null &&
                       request.getParameter("kepLink") != null){
                            List<String> szereplok = new ArrayList<String>();
                            List<String> rendezok = new ArrayList<String>();
                            szereplok.add("Jóska Pista");
                            szereplok.add("Kij Lóri");
                            rendezok.add("Hold Aladár");
                            rendezok.add("Nap Marcsi");
                            Integer hossz = Integer.parseInt(request.getParameter("hossz"));
                            Integer korhatar = Integer.parseInt(request.getParameter("korhatar"));
                            String cim = request.getParameter("cim").trim();
                            String leiras = request.getParameter("leiras").trim();
                            String kepLink = request.getParameter("kepLink").trim();
                            Film film = new Film(cim, szereplok, hossz, rendezok, korhatar, leiras, kepLink);
                            filmService.mentes(film);
                    }
                }
                if(request.getParameter("feladat").equals("osszes")){
                    List<Film> filmek = filmService.osszesFilm();
                    JSONArray osszesFilm = new JSONArray();
                    for(Film f : filmek){
                        JSONObject film = new JSONObject();
                        film.put("cim", f.getCim());
                        film.put("kepLink", f.getKepLink());
                        film.put("lekeresDatuma", new Date().toString());
                        osszesFilm.put(film);
                    }
                    out.write(osszesFilm.toString());
                }
            }//end of feladat not null
            
        }
        catch(Exception ex){
            System.out.println("Hiba: " + ex.toString());
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
