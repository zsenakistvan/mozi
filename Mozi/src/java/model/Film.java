package model;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import model.annotation.MinMaxValue;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class Film {
    
    public static final String fajlnev = "Filmek.xml";

    private String cim;
    private List<String> szereplok;
    @MinMaxValue(min = 0, max = 300)
    private Integer hossz;
    private List<String> rendezok;
    @MinMaxValue(min = 3, max = 24)
    private Integer korhatar;
    private String leiras;
    private String kepLink;

    public Film(String cim, List<String> szereplok, Integer hossz, List<String> rendezok, Integer korhatar, String leiras, String kepLink) {
        this.cim = cim;
        this.szereplok = szereplok;
        this.hossz = hossz;
        this.rendezok = rendezok;
        this.korhatar = korhatar;
        this.leiras = leiras;
        this.kepLink = kepLink;
    }
    
    public Film(String cim, String kepLink) {
        this.cim = cim;
        this.szereplok = null;
        this.hossz = null;
        this.rendezok = null;
        this.korhatar = null;
        this.leiras = null;
        this.kepLink = kepLink;
    }

    public String getCim() {
        return cim;
    }

    public List<String> getSzereplok() {
        return szereplok;
    }

    public Integer getHossz() {
        return hossz;
    }

    public List<String> getRendezok() {
        return rendezok;
    }

    public Integer getKorhatar() {
        return korhatar;
    }

    public String getLeiras() {
        return leiras;
    }

    public String getKepLink() {
        return kepLink;
    }

    public void setLeiras(String leiras) {
        this.leiras = leiras;
    }

    public void setKepLink(String kepLink) {
        this.kepLink = kepLink;
    }
    /*
    Film m = new Film("", new Arr);
    m.mentes();
    
    Film.mentes(m);
    */
    
    //public static void, hiszen nem ad vissza semmit
    //public static Integer, ami a mentett film Id-ját adja vissza (most nincs ID...)
    //public static Film, ami a mentett filmet adja vissza
    //public static Boolean, ami a true-t ad vissza, ha sikerült a mentés
    
    public static Boolean filmMentes(Film film){
        try{
            String filename = Film.fajlnev;
            File file = new File(filename);
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document xml = db.parse(file);
            xml.normalize();
            Element ujFilm = xml.createElement("film");
            Element cim = xml.createElement("cim");
            Element szereplok = xml.createElement("szereplok");
            Element hossz = xml.createElement("hossz");
            Element rendezok = xml.createElement("rendezok");
            Element korhatar = xml.createElement("korhatar");
            Element leiras = xml.createElement("leiras");
            Element kepLink = xml.createElement("kepLink");
            ujFilm.appendChild(cim);
            ujFilm.appendChild(szereplok);
            ujFilm.appendChild(hossz);
            ujFilm.appendChild(rendezok);
            ujFilm.appendChild(korhatar);
            ujFilm.appendChild(leiras);
            ujFilm.appendChild(kepLink);
            cim.setTextContent(film.getCim());
            hossz.setTextContent(film.getHossz().toString());
            korhatar.setTextContent(film.getKorhatar().toString());
            leiras.setTextContent(film.getLeiras());
            kepLink.setTextContent(film.getKepLink());
            for(String sz : film.getSzereplok()){
                Element szereplo = xml.createElement("szereplo");
                szereplok.appendChild(szereplo);
                szereplo.setTextContent(sz);
            }
            for(String r : film.getRendezok()){
                Element rendezo = xml.createElement("rendezo");
                rendezok.appendChild(rendezo);
                rendezo.setTextContent(r);
            }
            Node filmek = xml.getFirstChild();
            filmek.appendChild(ujFilm);
            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer t = tf.newTransformer();
            DOMSource source = new DOMSource(xml);
            StreamResult result = new StreamResult(file);
            t.transform(source, result);
            return Boolean.TRUE;
        }
        catch(Exception ex){
            System.out.println("Hiba: " + ex.toString());
        }
        return Boolean.FALSE;
    }
    
    public static List<Film> osszesFilm(){
        List<Film> filmek = new ArrayList<>();
        try{
            String filename = Film.fajlnev;
            File file = new File(filename);
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document xml = db.parse(file);
            xml.normalize();
            NodeList lista = xml.getElementsByTagName("film");
            for(Integer i = 0; i < lista.getLength(); i++){
                Node egyed = lista.item(i);
                Element film = (Element)egyed;
                String cim = film.getElementsByTagName("cim").item(0).getTextContent();
                String kepLink = film.getElementsByTagName("kepLink").item(0).getTextContent();
                Film f = new Film(cim, kepLink);
                filmek.add(f);
            }
        }
        catch(Exception ex){
            System.out.println("Hiba: " + ex.toString());
        }
        return filmek;
    }
    
}
