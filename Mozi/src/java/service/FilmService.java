package service;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import model.Film;
import model.annotation.MinMaxValue;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class FilmService {
    
    public Boolean mentes(Film film){
        try{
            Integer minHossz = film.getClass().getDeclaredField("hossz").getAnnotation(MinMaxValue.class).min();
            Integer maxHossz = film.getClass().getDeclaredField("hossz").getAnnotation(MinMaxValue.class).max();
            Integer minKorhatar = film.getClass().getDeclaredField("korhatar").getAnnotation(MinMaxValue.class).min();
            Integer maxKorhatar = film.getClass().getDeclaredField("korhatar").getAnnotation(MinMaxValue.class).max();
            if(film.getKorhatar() < minKorhatar ||
               film.getKorhatar() > maxKorhatar ||
               film.getHossz() < minHossz ||
               film.getHossz() > maxHossz){
                    return Boolean.FALSE;
            }
            return Film.filmMentes(film);
        }
        catch(Exception ex){
            System.out.println("Hiba: " + ex.toString());
        }
        return Boolean.FALSE;
    }
    
    public List<Film> osszesFilm(){
        List<Film> filmek = new ArrayList<>();
        try{
           filmek = Film.osszesFilm();
        }
        catch(Exception ex){
            System.out.println("Hiba: " + ex.toString());
        }
        return filmek;
    }
    
    
    
}